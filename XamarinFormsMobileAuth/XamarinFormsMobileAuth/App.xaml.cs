using System;
using Xam.Plugin.Validators;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace XamarinFormsMobileAuth
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();
			Validation.Init();

			MainPage = new Login();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
