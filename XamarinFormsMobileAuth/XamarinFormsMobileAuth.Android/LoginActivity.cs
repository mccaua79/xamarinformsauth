﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace XamarinFormsMobileAuth.Droid
{
	[Activity( Label = "XamarinFormsMobileAuth", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation )]
	public class LoginActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate( Bundle bundle )
		{
			base.OnCreate( bundle );

			// Create your application here
			global::Xamarin.Forms.Forms.Init( this, bundle );
			LoadApplication( new App() );
		}
	}
}